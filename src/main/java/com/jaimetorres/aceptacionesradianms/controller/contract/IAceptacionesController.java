package com.jaimetorres.aceptacionesradianms.controller.contract;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.jaimetorres.aceptacionesradianms.types.AceptacionExpresaType;
import com.jaimetorres.aceptacionesradianms.types.AceptacionTacitaType;
import com.jaimetorres.aceptacionesradianms.types.ReclamoFacturaType;
import com.jaimetorres.aceptacionesradianms.types.ApplicationResponses.ApplicationResponse;
import com.jaimetorres.aceptacionesradianms.utils.validator.RespuestaDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

public interface IAceptacionesController {

	@Operation(method = "crearAceptacionExpresa", operationId = "crearAceptacionExpresa", description = "Capacidad que se encarga de crear la aceptacion expresa ", tags = "AceptacionesRadianMS", summary = "crearAceptacionExpresa")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/ms/inscripciontitulovalor/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = ApplicationResponse.class)))
	public ResponseEntity<Object> crearInscripcionFacturaComoTituloValor(@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) ApplicationResponse inscripcionFacturaTituloValor);

	@Operation(method = "crearAceptacionExpresa", operationId = "crearAceptacionExpresa", description = "Capacidad que se encarga de crear la aceptacion expresa ", tags = "AceptacionesRadianMS", summary = "crearAceptacionExpresa")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/ms/aceptacionexpresa/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = String.class)))
	public ResponseEntity<Object> crearAceptacionExpresa(@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) AceptacionExpresaType aceptacionExpresaType);

	@Operation(method = "crearAceptacionTacita", operationId = "crearAceptacionTacita", description = "Capacidad que se encarga de crear la aceptacion tacita ", tags = "AceptacionesRadianMS", summary = "crearAceptacionTacita")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/ms/aceptaciontacita/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = String.class)))
	public ResponseEntity<Object> crearAceptacionTacita(@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) AceptacionTacitaType aceptacionTacitaType);

	@Operation(method = "crearAceptacionExpresa", operationId = "crearAceptacionExpresa", description = "Capacidad que se encarga de crear la aceptacion expresa ", tags = "AceptacionesRadianMS", summary = "crearAceptacionExpresa")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/ms/reclamo/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = String.class)))
	public ResponseEntity<Object> crearReclamoRadian(@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) ReclamoFacturaType reclamoFactura);

}
