package com.jaimetorres.aceptacionesradianms.controller.impl;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.jaimetorres.aceptacionesradianms.controller.contract.IAceptacionesController;
import com.jaimetorres.aceptacionesradianms.service.contract.IAceptacionesSvc;
import com.jaimetorres.aceptacionesradianms.types.AceptacionExpresaType;
import com.jaimetorres.aceptacionesradianms.types.AceptacionTacitaType;
import com.jaimetorres.aceptacionesradianms.types.ReclamoFacturaType;
import com.jaimetorres.aceptacionesradianms.types.ApplicationResponses.ApplicationResponse;

@RestController
public class AceptacionesControllerImpl implements IAceptacionesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AceptacionesControllerImpl.class);

	private static final String capaController = "CAPA_DEL_CONTROLADOR";

	@Autowired
	IAceptacionesSvc serviceAceptaciones;

	@Override
	public ResponseEntity<Object> crearAceptacionExpresa(@Valid AceptacionExpresaType aceptacionExpresaType) {
		LOGGER.info("INICIA CREAR ACEPTACION EXPRESA" + capaController);
		try {

		} catch (Exception e) {
			LOGGER.info("ERROR EN CAPACIDAD DE CREAR ACEPTACION EXPRESA " + e.toString());
		}
		LOGGER.info("FINALIZA CREAR ACEPTACION EXPRESA" + capaController);
		return null;
	}

	@Override
	public ResponseEntity<Object> crearAceptacionTacita(@Valid AceptacionTacitaType aceptacionTacitaType) {
		LOGGER.info("INICIA CREAR ACEPTACION EXPRESA" + capaController);
		try {

		} catch (Exception e) {
			LOGGER.info("ERROR EN CAPACIDAD DE CREAR ACEPTACION TACITA " + e.toString());
		}
		LOGGER.info("FINALIZA CREAR ACEPTACION EXPRESA" + capaController);
		return null;
	}

	@Override
	public ResponseEntity<Object> crearReclamoRadian(@Valid ReclamoFacturaType reclamoFactura) {
		LOGGER.info("INICIA CREAR ACEPTACION EXPRESA" + capaController);
		try {

		} catch (Exception e) {
			LOGGER.info("ERROR EN CAPACIDAD DE CREAR RECLAMO " + e.toString());
		}
		LOGGER.info("FINALIZA CREAR ACEPTACION EXPRESA" + capaController);
		return null;
	}

	@Override
	public ResponseEntity<Object> crearInscripcionFacturaComoTituloValor(
			@Valid ApplicationResponse inscripcionFacturaTituloValor) {
		// TODO Auto-generated method stub
		return null;
	}

}
