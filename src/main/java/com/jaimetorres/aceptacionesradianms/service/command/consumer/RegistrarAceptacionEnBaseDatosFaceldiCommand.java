package com.jaimetorres.aceptacionesradianms.service.command.consumer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jaimetorres.aceptacionesradianms.controller.constans.AceptacionRadianConstans;
import com.jaimetorres.aceptacionesradianms.types.ApplicationResponseType;
import com.jaimetorres.aceptacionesradianms.utils.exception.BusinessException;
import com.jaimetorres.aceptacionesradianms.utils.patronCommand.ICommand;
import com.jaimetorres.aceptacionesradianms.utils.patronCommand.IParameter;

@Component
public class RegistrarAceptacionEnBaseDatosFaceldiCommand implements ICommand {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrarAceptacionEnBaseDatosFaceldiCommand.class);

	@Value("${uri.url}")
	private String url;

	@Value("${uri.user}")
	private String user;

	@Value("${uri.pass}")
	private String pass;

	@Override
	public Object execute(IParameter parameter) throws BusinessException {
		LOGGER.info("INICIA COMANDO REGISTRAR ACEPTACION EN BASE DE DATOS FACELDI");
		ApplicationResponseType applicationResponse = (ApplicationResponseType) parameter;

		try {
			Connection con = DriverManager.getConnection(url, user, pass);
		
			PreparedStatement stamentInsercion = con
					.prepareStatement(AceptacionRadianConstans.INSERT_APLICATION_RESPONSE);
			// ID DE SUCURSAL
			stamentInsercion.setInt(1, 1);
			// NUMERO DEL EVENTO
			stamentInsercion.setInt(2, applicationResponse.getNumeroEvento());
			// CODIGO DEL EEVNTO
			stamentInsercion.setInt(3, applicationResponse.getCudeEvento());
			// VALOR DE LA FACTURA
			stamentInsercion.setDouble(4, 8);
			// FECHA DEL EVENTO
			stamentInsercion.setInt(5, 5);
			// CUFE
			stamentInsercion.setDouble(6, applicationResponse.getCufeFacturaAsociada());
			// APELLIDOS EMISOR ELECTRONICO
			stamentInsercion.setString(7, applicationResponse.getApellidosFuncionario());

			// NOMBRES DEL EMISOR ELECTRONICO
			stamentInsercion.setString(8, applicationResponse.getNombreFuncionariosRecibirBien());
			// NIT DEL EMISOR ELECTRONICO
			stamentInsercion.setString(9, applicationResponse.getNumeroIdentificacion());
			// FECHA REGISTRO DEL EVENTO
			// stamentInsercion.setDate(10, new DateSchema());

			// TIPO DE ACEPTACION

			// ESTADO DEL PAGO

			// TIPO DE NEGOCIOACION

			// APELLIDOS Y NOMBRE DEL COMPRADOR

			// FIRMA DIGIRAL EMISOR

			// TIPO DE ENTIDAD

			stamentInsercion.execute();

		} catch (Exception e) {
			LOGGER.info("Error en insercion liquidacion repository" + e.getMessage());
		} 
		LOGGER.info("FINALIZA COMANDO REGISTRAR ACEPTACION EN BASE DE DATOS FACELDI");
		return null;
	}

	@Override
	public void undo() {
		throw new UnsupportedOperationException();
	}

}
