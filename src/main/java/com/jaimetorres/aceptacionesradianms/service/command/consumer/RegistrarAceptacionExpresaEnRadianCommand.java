package com.jaimetorres.aceptacionesradianms.service.command.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jaimetorres.aceptacionesradianms.utils.exception.BusinessException;
import com.jaimetorres.aceptacionesradianms.utils.patronCommand.ICommand;
import com.jaimetorres.aceptacionesradianms.utils.patronCommand.IParameter;

@Component
public class RegistrarAceptacionExpresaEnRadianCommand implements ICommand {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrarAceptacionExpresaEnRadianCommand.class);

	@Override
	public Object execute(IParameter parameter) throws BusinessException {
		LOGGER.info("CREAR COMANDO CREAR ACEPTACION EXPRESA RADIAN SERVICE");
		
		LOGGER.info("FINALIZA COMANDO CREAR ACEPTACION EXPRESA RADIAN SERVICE");
		return null;
	}

	@Override
	public void undo() {
		throw new UnsupportedOperationException();
	}

}
