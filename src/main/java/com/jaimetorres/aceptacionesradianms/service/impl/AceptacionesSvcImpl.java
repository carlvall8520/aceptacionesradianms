package com.jaimetorres.aceptacionesradianms.service.impl;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaimetorres.aceptacionesradianms.service.command.consumer.CrearAceptacionTacitaRadianServiceCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.CrearReclamoRadianServiceCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.RegistrarAceptacionEnBaseDatosFaceldiCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.RegistrarAceptacionExpresaEnRadianCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.ValidarExistenciaAceptacionesExpresasCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.ValidarExistenciaDeAceptacionesTacitasCommand;
import com.jaimetorres.aceptacionesradianms.service.command.consumer.ValidarReclamosPendientesDeFacturaCommand;
import com.jaimetorres.aceptacionesradianms.service.contract.IAceptacionesSvc;
import com.jaimetorres.aceptacionesradianms.types.AceptacionExpresaType;
import com.jaimetorres.aceptacionesradianms.types.AceptacionTacitaType;
import com.jaimetorres.aceptacionesradianms.types.ReclamoFacturaType;
import com.jaimetorres.aceptacionesradianms.utils.exception.BusinessException;

@Service
public class AceptacionesSvcImpl implements IAceptacionesSvc {

	private static final Logger LOGGER = LoggerFactory.getLogger(AceptacionesSvcImpl.class);

	private static final String capaService = "CAPA_SERVICIO";

	@Autowired
	CrearAceptacionTacitaRadianServiceCommand crearAceptacionTacitaRadianServiceCommand;

	@Autowired
	CrearReclamoRadianServiceCommand crearReclamoRadianServiceCommand;

	@Autowired
	RegistrarAceptacionEnBaseDatosFaceldiCommand registrarAplicationResponseEnFaceldi;

	@Autowired
	RegistrarAceptacionExpresaEnRadianCommand registrarAceptacionExpresaRadianCommand;

	@Autowired
	ValidarExistenciaDeAceptacionesTacitasCommand validarExistenciaAceptacionesTacitas;

	@Autowired
	ValidarExistenciaAceptacionesExpresasCommand validarExistenciasAceptacionesExpresasCommand;

	@Autowired
	ValidarReclamosPendientesDeFacturaCommand validarReclamosPendienteCommand;
	
	

	@Override
	public String crearAceptacionExpresa(@Valid AceptacionExpresaType aceptacionExpresaType) throws BusinessException {
		LOGGER.info("INICIA CREAR ACEPTACION EXPRESA" + capaService);
		LOGGER.info("ACEPTACION EXPRESA TYPE ->" + aceptacionExpresaType.toString());

		validarReclamosPendienteCommand.execute(null);

		validarExistenciaAceptacionesTacitas.execute(null);

		registrarAceptacionExpresaRadianCommand.execute(null);

		registrarAceptacionExpresaRadianCommand.execute(null);

		LOGGER.info("FINALIZA CREAR ACEPTACION EXPRESA" + capaService);
		return null;
	}

	@Override
	public String crearAceptacionTacita(@Valid AceptacionTacitaType aceptacionTacitaType) throws BusinessException {
		LOGGER.info("INICIA CREAR ACEPTACION TACITA" + capaService);
		LOGGER.info("ACEPTACION TACITA TYPE ->" + aceptacionTacitaType.toString());

		validarExistenciaAceptacionesTacitas.execute(null);

		validarExistenciasAceptacionesExpresasCommand.execute(null);

		registrarAplicationResponseEnFaceldi.execute(null);

		crearAceptacionTacitaRadianServiceCommand.execute(null);

		LOGGER.info("FINALIZA CREAR ACEPTACION TACITA" + capaService);
		return null;
	}

	@Override
	public String crearReclamoRadian(@Valid ReclamoFacturaType reclamoFactura) throws BusinessException {
		LOGGER.info("INICIA CREAR RECLAMO RADIAN" + capaService);
		LOGGER.info("RECLAMO RADIAN TYPE ->" + reclamoFactura.toString());

		validarExistenciaAceptacionesTacitas.execute(null);

		validarExistenciasAceptacionesExpresasCommand.execute(null);

		crearReclamoRadianServiceCommand.execute(null);

		registrarAplicationResponseEnFaceldi.execute(null);

		LOGGER.info("FINALIZA CREAR RECLAMO RADIAN" + capaService);
		return null;
	}

}
