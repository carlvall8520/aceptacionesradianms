package com.jaimetorres.aceptacionesradianms.service.contract;

import javax.validation.Valid;

import com.jaimetorres.aceptacionesradianms.types.AceptacionExpresaType;
import com.jaimetorres.aceptacionesradianms.types.AceptacionTacitaType;
import com.jaimetorres.aceptacionesradianms.types.ReclamoFacturaType;
import com.jaimetorres.aceptacionesradianms.utils.exception.BusinessException;

public interface IAceptacionesSvc {

	public String crearAceptacionExpresa(@Valid AceptacionExpresaType aceptacionExpresaType) throws BusinessException;

	public String crearAceptacionTacita(@Valid AceptacionTacitaType aceptacionTacitaType) throws BusinessException;

	public String crearReclamoRadian(@Valid ReclamoFacturaType reclamoFactura) throws BusinessException;

}
