package com.jaimetorres.aceptacionesradianms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AceptacionesradianmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AceptacionesradianmsApplication.class, args);
	}

}
