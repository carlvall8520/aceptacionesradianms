package com.jaimetorres.aceptacionesradianms.types;

import java.util.Date;

public class ApplicationResponseType {

	private String apellidosFuncionario;
	private String contenedorElectronico;
	private int cudeEvento;
	private int cufeFacturaAsociada;
	private Date fechaHoraReciboDelBien;
	private int id;
	private String nombreFuncionariosRecibirBien;
	private int numeroEvento;
	private String numeroIdentificacion;
	private String observaciones;
	private String tipoIdentificacionFuncionarioEncargado;

	/**
	 * @return the apellidosFuncionario
	 */
	public String getApellidosFuncionario() {
		return apellidosFuncionario;
	}

	/**
	 * @param apellidosFuncionario the apellidosFuncionario to set
	 */
	public void setApellidosFuncionario(String apellidosFuncionario) {
		this.apellidosFuncionario = apellidosFuncionario;
	}

	/**
	 * @return the contenedorElectronico
	 */
	public String getContenedorElectronico() {
		return contenedorElectronico;
	}

	/**
	 * @param contenedorElectronico the contenedorElectronico to set
	 */
	public void setContenedorElectronico(String contenedorElectronico) {
		this.contenedorElectronico = contenedorElectronico;
	}

	/**
	 * @return the cudeEvento
	 */
	public int getCudeEvento() {
		return cudeEvento;
	}

	/**
	 * @param cudeEvento the cudeEvento to set
	 */
	public void setCudeEvento(int cudeEvento) {
		this.cudeEvento = cudeEvento;
	}

	/**
	 * @return the cufeFacturaAsociada
	 */
	public int getCufeFacturaAsociada() {
		return cufeFacturaAsociada;
	}

	/**
	 * @param cufeFacturaAsociada the cufeFacturaAsociada to set
	 */
	public void setCufeFacturaAsociada(int cufeFacturaAsociada) {
		this.cufeFacturaAsociada = cufeFacturaAsociada;
	}

	/**
	 * @return the fechaHoraReciboDelBien
	 */
	public Date getFechaHoraReciboDelBien() {
		return fechaHoraReciboDelBien;
	}

	/**
	 * @param fechaHoraReciboDelBien the fechaHoraReciboDelBien to set
	 */
	public void setFechaHoraReciboDelBien(Date fechaHoraReciboDelBien) {
		this.fechaHoraReciboDelBien = fechaHoraReciboDelBien;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nombreFuncionariosRecibirBien
	 */
	public String getNombreFuncionariosRecibirBien() {
		return nombreFuncionariosRecibirBien;
	}

	/**
	 * @param nombreFuncionariosRecibirBien the nombreFuncionariosRecibirBien to set
	 */
	public void setNombreFuncionariosRecibirBien(String nombreFuncionariosRecibirBien) {
		this.nombreFuncionariosRecibirBien = nombreFuncionariosRecibirBien;
	}

	/**
	 * @return the numeroEvento
	 */
	public int getNumeroEvento() {
		return numeroEvento;
	}

	/**
	 * @param numeroEvento the numeroEvento to set
	 */
	public void setNumeroEvento(int numeroEvento) {
		this.numeroEvento = numeroEvento;
	}

	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	/**
	 * @param numeroIdentificacion the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the tipoIdentificacionFuncionarioEncargado
	 */
	public String getTipoIdentificacionFuncionarioEncargado() {
		return tipoIdentificacionFuncionarioEncargado;
	}

	/**
	 * @param tipoIdentificacionFuncionarioEncargado the
	 *                                               tipoIdentificacionFuncionarioEncargado
	 *                                               to set
	 */
	public void setTipoIdentificacionFuncionarioEncargado(String tipoIdentificacionFuncionarioEncargado) {
		this.tipoIdentificacionFuncionarioEncargado = tipoIdentificacionFuncionarioEncargado;
	}

}
