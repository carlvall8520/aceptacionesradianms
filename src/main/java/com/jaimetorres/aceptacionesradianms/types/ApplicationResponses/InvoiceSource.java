package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class InvoiceSource {

	public IdentificationCode IdentificationCode;

	/**
	 * @return the identificationCode
	 */
	public IdentificationCode getIdentificationCode() {
		return IdentificationCode;
	}

	/**
	 * @param identificationCode the identificationCode to set
	 */
	public void setIdentificationCode(IdentificationCode identificationCode) {
		IdentificationCode = identificationCode;
	}

	@Override
	public String toString() {
		return "InvoiceSource [IdentificationCode=" + IdentificationCode + "]";
	}

}
