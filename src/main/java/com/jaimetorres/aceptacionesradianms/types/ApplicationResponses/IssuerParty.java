package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class IssuerParty {

	public Person Person;

	/**
	 * @return the person
	 */
	public Person getPerson() {
		return Person;
	}

	/**
	 * @param person the person to set
	 */
	public void setPerson(Person person) {
		Person = person;
	}

}
