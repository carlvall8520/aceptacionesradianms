package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class CompanyID {
	public int schemeAgencyID;
	public String schemeAgencyName;
	public int schemeID;
	public int schemeName;
	public int schemeVersionID;
	public int text;

	/**
	 * @return the schemeAgencyID
	 */
	public int getSchemeAgencyID() {
		return schemeAgencyID;
	}

	/**
	 * @param schemeAgencyID the schemeAgencyID to set
	 */
	public void setSchemeAgencyID(int schemeAgencyID) {
		this.schemeAgencyID = schemeAgencyID;
	}

	/**
	 * @return the schemeAgencyName
	 */
	public String getSchemeAgencyName() {
		return schemeAgencyName;
	}

	/**
	 * @param schemeAgencyName the schemeAgencyName to set
	 */
	public void setSchemeAgencyName(String schemeAgencyName) {
		this.schemeAgencyName = schemeAgencyName;
	}

	/**
	 * @return the schemeID
	 */
	public int getSchemeID() {
		return schemeID;
	}

	/**
	 * @param schemeID the schemeID to set
	 */
	public void setSchemeID(int schemeID) {
		this.schemeID = schemeID;
	}

	/**
	 * @return the schemeName
	 */
	public int getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName the schemeName to set
	 */
	public void setSchemeName(int schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return the schemeVersionID
	 */
	public int getSchemeVersionID() {
		return schemeVersionID;
	}

	/**
	 * @param schemeVersionID the schemeVersionID to set
	 */
	public void setSchemeVersionID(int schemeVersionID) {
		this.schemeVersionID = schemeVersionID;
	}

	/**
	 * @return the text
	 */
	public int getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(int text) {
		this.text = text;
	}

}
