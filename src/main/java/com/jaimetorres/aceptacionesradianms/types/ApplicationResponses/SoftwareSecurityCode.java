package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class SoftwareSecurityCode {
	public int schemeAgencyID;
	public String schemeAgencyName;
	public String text;

	/**
	 * @return the schemeAgencyID
	 */
	public int getSchemeAgencyID() {
		return schemeAgencyID;
	}

	/**
	 * @param schemeAgencyID the schemeAgencyID to set
	 */
	public void setSchemeAgencyID(int schemeAgencyID) {
		this.schemeAgencyID = schemeAgencyID;
	}

	/**
	 * @return the schemeAgencyName
	 */
	public String getSchemeAgencyName() {
		return schemeAgencyName;
	}

	/**
	 * @param schemeAgencyName the schemeAgencyName to set
	 */
	public void setSchemeAgencyName(String schemeAgencyName) {
		this.schemeAgencyName = schemeAgencyName;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "SoftwareSecurityCode [schemeAgencyID=" + schemeAgencyID + ", schemeAgencyName=" + schemeAgencyName
				+ ", text=" + text + "]";
	}

}
