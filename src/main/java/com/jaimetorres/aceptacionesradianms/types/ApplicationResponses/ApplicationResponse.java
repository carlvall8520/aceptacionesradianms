package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

import java.util.Date;
import java.util.List;

public class ApplicationResponse {

	private UBLExtensions UBLExtensions;
	private String UBLVersionID;
	private int CustomizationID;
	private String ProfileID;
	private int ProfileExecutionID;
	private String ID;
	private UUID UUID;
	private Date IssueDate;
	private Date IssueTime;
	private List<String> Note;
	private SenderParty SenderParty;
	private ReceiverParty ReceiverParty;
	private DocumentResponse DocumentResponse;
	private String xmlns;
	private String cac;
	private String cbc;
	private String ds;
	private String ext;
	private String sts;
	private String xades;
	private String xades141;
	private String xsi;
	private String schemaLocation;
	private String text;

	/**
	 * @return the uBLExtensions
	 */
	public UBLExtensions getUBLExtensions() {
		return UBLExtensions;
	}

	/**
	 * @param uBLExtensions the uBLExtensions to set
	 */
	public void setUBLExtensions(UBLExtensions uBLExtensions) {
		UBLExtensions = uBLExtensions;
	}

	/**
	 * @return the uBLVersionID
	 */
	public String getUBLVersionID() {
		return UBLVersionID;
	}

	/**
	 * @param uBLVersionID the uBLVersionID to set
	 */
	public void setUBLVersionID(String uBLVersionID) {
		UBLVersionID = uBLVersionID;
	}

	/**
	 * @return the customizationID
	 */
	public int getCustomizationID() {
		return CustomizationID;
	}

	/**
	 * @param customizationID the customizationID to set
	 */
	public void setCustomizationID(int customizationID) {
		CustomizationID = customizationID;
	}

	/**
	 * @return the profileID
	 */
	public String getProfileID() {
		return ProfileID;
	}

	/**
	 * @param profileID the profileID to set
	 */
	public void setProfileID(String profileID) {
		ProfileID = profileID;
	}

	/**
	 * @return the profileExecutionID
	 */
	public int getProfileExecutionID() {
		return ProfileExecutionID;
	}

	/**
	 * @param profileExecutionID the profileExecutionID to set
	 */
	public void setProfileExecutionID(int profileExecutionID) {
		ProfileExecutionID = profileExecutionID;
	}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the uUID
	 */
	public UUID getUUID() {
		return UUID;
	}

	/**
	 * @param uUID the uUID to set
	 */
	public void setUUID(UUID uUID) {
		UUID = uUID;
	}

	/**
	 * @return the issueDate
	 */
	public Date getIssueDate() {
		return IssueDate;
	}

	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(Date issueDate) {
		IssueDate = issueDate;
	}

	/**
	 * @return the issueTime
	 */
	public Date getIssueTime() {
		return IssueTime;
	}

	/**
	 * @param issueTime the issueTime to set
	 */
	public void setIssueTime(Date issueTime) {
		IssueTime = issueTime;
	}

	/**
	 * @return the note
	 */
	public List<String> getNote() {
		return Note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(List<String> note) {
		Note = note;
	}

	/**
	 * @return the senderParty
	 */
	public SenderParty getSenderParty() {
		return SenderParty;
	}

	/**
	 * @param senderParty the senderParty to set
	 */
	public void setSenderParty(SenderParty senderParty) {
		SenderParty = senderParty;
	}

	/**
	 * @return the receiverParty
	 */
	public ReceiverParty getReceiverParty() {
		return ReceiverParty;
	}

	/**
	 * @param receiverParty the receiverParty to set
	 */
	public void setReceiverParty(ReceiverParty receiverParty) {
		ReceiverParty = receiverParty;
	}

	/**
	 * @return the documentResponse
	 */
	public DocumentResponse getDocumentResponse() {
		return DocumentResponse;
	}

	/**
	 * @param documentResponse the documentResponse to set
	 */
	public void setDocumentResponse(DocumentResponse documentResponse) {
		DocumentResponse = documentResponse;
	}

	/**
	 * @return the xmlns
	 */
	public String getXmlns() {
		return xmlns;
	}

	/**
	 * @param xmlns the xmlns to set
	 */
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	/**
	 * @return the cac
	 */
	public String getCac() {
		return cac;
	}

	/**
	 * @param cac the cac to set
	 */
	public void setCac(String cac) {
		this.cac = cac;
	}

	/**
	 * @return the cbc
	 */
	public String getCbc() {
		return cbc;
	}

	/**
	 * @param cbc the cbc to set
	 */
	public void setCbc(String cbc) {
		this.cbc = cbc;
	}

	/**
	 * @return the ds
	 */
	public String getDs() {
		return ds;
	}

	/**
	 * @param ds the ds to set
	 */
	public void setDs(String ds) {
		this.ds = ds;
	}

	/**
	 * @return the ext
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * @param ext the ext to set
	 */
	public void setExt(String ext) {
		this.ext = ext;
	}

	/**
	 * @return the sts
	 */
	public String getSts() {
		return sts;
	}

	/**
	 * @param sts the sts to set
	 */
	public void setSts(String sts) {
		this.sts = sts;
	}

	/**
	 * @return the xades
	 */
	public String getXades() {
		return xades;
	}

	/**
	 * @param xades the xades to set
	 */
	public void setXades(String xades) {
		this.xades = xades;
	}

	/**
	 * @return the xades141
	 */
	public String getXades141() {
		return xades141;
	}

	/**
	 * @param xades141 the xades141 to set
	 */
	public void setXades141(String xades141) {
		this.xades141 = xades141;
	}

	/**
	 * @return the xsi
	 */
	public String getXsi() {
		return xsi;
	}

	/**
	 * @param xsi the xsi to set
	 */
	public void setXsi(String xsi) {
		this.xsi = xsi;
	}

	/**
	 * @return the schemaLocation
	 */
	public String getSchemaLocation() {
		return schemaLocation;
	}

	/**
	 * @param schemaLocation the schemaLocation to set
	 */
	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "ApplicationResponse [UBLExtensions=" + UBLExtensions + ", UBLVersionID=" + UBLVersionID
				+ ", CustomizationID=" + CustomizationID + ", ProfileID=" + ProfileID + ", ProfileExecutionID="
				+ ProfileExecutionID + ", ID=" + ID + ", UUID=" + UUID + ", IssueDate=" + IssueDate + ", IssueTime="
				+ IssueTime + ", Note=" + Note + ", SenderParty=" + SenderParty + ", ReceiverParty=" + ReceiverParty
				+ ", DocumentResponse=" + DocumentResponse + ", xmlns=" + xmlns + ", cac=" + cac + ", cbc=" + cbc
				+ ", ds=" + ds + ", ext=" + ext + ", sts=" + sts + ", xades=" + xades + ", xades141=" + xades141
				+ ", xsi=" + xsi + ", schemaLocation=" + schemaLocation + ", text=" + text + "]";
	}

}
