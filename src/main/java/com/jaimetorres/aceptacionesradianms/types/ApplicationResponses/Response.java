package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class Response {
	public int ResponseCode;
	public String Description;

	/**
	 * @return the responseCode
	 */
	public int getResponseCode() {
		return ResponseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(int responseCode) {
		ResponseCode = responseCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}

}
