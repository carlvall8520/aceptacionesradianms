package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class SenderParty {

	public PartyTaxScheme PartyTaxScheme;

	/**
	 * @return the partyTaxScheme
	 */
	public PartyTaxScheme getPartyTaxScheme() {
		return PartyTaxScheme;
	}

	/**
	 * @param partyTaxScheme the partyTaxScheme to set
	 */
	public void setPartyTaxScheme(PartyTaxScheme partyTaxScheme) {
		PartyTaxScheme = partyTaxScheme;
	}

}
