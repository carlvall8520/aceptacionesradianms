package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class ID {

	public int schemeID;
	public int schemeName;
	public double text;

	/**
	 * @return the schemeID
	 */
	public int getSchemeID() {
		return schemeID;
	}

	/**
	 * @param schemeID the schemeID to set
	 */
	public void setSchemeID(int schemeID) {
		this.schemeID = schemeID;
	}

	/**
	 * @return the schemeName
	 */
	public int getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName the schemeName to set
	 */
	public void setSchemeName(int schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return the text
	 */
	public double getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(double text) {
		this.text = text;
	}

}
