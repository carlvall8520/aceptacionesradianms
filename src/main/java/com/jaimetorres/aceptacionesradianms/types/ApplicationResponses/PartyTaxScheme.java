package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class PartyTaxScheme {

	public String RegistrationName;
	public CompanyID CompanyID;
	public TaxScheme TaxScheme;

	/**
	 * @return the registrationName
	 */
	public String getRegistrationName() {
		return RegistrationName;
	}

	/**
	 * @param registrationName the registrationName to set
	 */
	public void setRegistrationName(String registrationName) {
		RegistrationName = registrationName;
	}

	/**
	 * @return the companyID
	 */
	public CompanyID getCompanyID() {
		return CompanyID;
	}

	/**
	 * @param companyID the companyID to set
	 */
	public void setCompanyID(CompanyID companyID) {
		CompanyID = companyID;
	}

	/**
	 * @return the taxScheme
	 */
	public TaxScheme getTaxScheme() {
		return TaxScheme;
	}

	/**
	 * @param taxScheme the taxScheme to set
	 */
	public void setTaxScheme(TaxScheme taxScheme) {
		TaxScheme = taxScheme;
	}

	@Override
	public String toString() {
		return "PartyTaxScheme [RegistrationName=" + RegistrationName + ", CompanyID=" + CompanyID + ", TaxScheme="
				+ TaxScheme + "]";
	}

}
