package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class UBLExtension {
	public ExtensionContent ExtensionContent;

	/**
	 * @return the extensionContent
	 */
	public ExtensionContent getExtensionContent() {
		return ExtensionContent;
	}

	/**
	 * @param extensionContent the extensionContent to set
	 */
	public void setExtensionContent(ExtensionContent extensionContent) {
		ExtensionContent = extensionContent;
	}

}
