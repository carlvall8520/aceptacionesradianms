package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class ExtensionContent {
	public DianExtensions DianExtensions;

	/**
	 * @return the dianExtensions
	 */
	public DianExtensions getDianExtensions() {
		return DianExtensions;
	}

	/**
	 * @param dianExtensions the dianExtensions to set
	 */
	public void setDianExtensions(DianExtensions dianExtensions) {
		DianExtensions = dianExtensions;
	}

	@Override
	public String toString() {
		return "ExtensionContent [DianExtensions=" + DianExtensions + "]";
	}

}
