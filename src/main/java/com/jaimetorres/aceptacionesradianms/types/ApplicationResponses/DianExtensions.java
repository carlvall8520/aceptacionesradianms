package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class DianExtensions {
	public InvoiceSource InvoiceSource;
	public SoftwareProvider SoftwareProvider;
	public SoftwareSecurityCode SoftwareSecurityCode;
	public AuthorizationProvider AuthorizationProvider;
	public String QRCode;

	/**
	 * @return the invoiceSource
	 */
	public InvoiceSource getInvoiceSource() {
		return InvoiceSource;
	}

	/**
	 * @param invoiceSource the invoiceSource to set
	 */
	public void setInvoiceSource(InvoiceSource invoiceSource) {
		InvoiceSource = invoiceSource;
	}

	/**
	 * @return the softwareProvider
	 */
	public SoftwareProvider getSoftwareProvider() {
		return SoftwareProvider;
	}

	/**
	 * @param softwareProvider the softwareProvider to set
	 */
	public void setSoftwareProvider(SoftwareProvider softwareProvider) {
		SoftwareProvider = softwareProvider;
	}

	/**
	 * @return the softwareSecurityCode
	 */
	public SoftwareSecurityCode getSoftwareSecurityCode() {
		return SoftwareSecurityCode;
	}

	/**
	 * @param softwareSecurityCode the softwareSecurityCode to set
	 */
	public void setSoftwareSecurityCode(SoftwareSecurityCode softwareSecurityCode) {
		SoftwareSecurityCode = softwareSecurityCode;
	}

	/**
	 * @return the authorizationProvider
	 */
	public AuthorizationProvider getAuthorizationProvider() {
		return AuthorizationProvider;
	}

	/**
	 * @param authorizationProvider the authorizationProvider to set
	 */
	public void setAuthorizationProvider(AuthorizationProvider authorizationProvider) {
		AuthorizationProvider = authorizationProvider;
	}

	/**
	 * @return the qRCode
	 */
	public String getQRCode() {
		return QRCode;
	}

	/**
	 * @param qRCode the qRCode to set
	 */
	public void setQRCode(String qRCode) {
		QRCode = qRCode;
	}

	@Override
	public String toString() {
		return "DianExtensions [InvoiceSource=" + InvoiceSource + ", SoftwareProvider=" + SoftwareProvider
				+ ", SoftwareSecurityCode=" + SoftwareSecurityCode + ", AuthorizationProvider=" + AuthorizationProvider
				+ ", QRCode=" + QRCode + "]";
	}

}
