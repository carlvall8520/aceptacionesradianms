package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class TaxScheme {
	public int ID;
	public String Name;

	@Override
	public String toString() {
		return "TaxScheme [ID=" + ID + ", Name=" + Name + "]";
	}

}
