package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class DocumentResponse {

	public Response Response;
	public DocumentReference DocumentReference;
	public IssuerParty IssuerParty;

	/**
	 * @return the response
	 */
	public Response getResponse() {
		return Response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(Response response) {
		Response = response;
	}

	/**
	 * @return the documentReference
	 */
	public DocumentReference getDocumentReference() {
		return DocumentReference;
	}

	/**
	 * @param documentReference the documentReference to set
	 */
	public void setDocumentReference(DocumentReference documentReference) {
		DocumentReference = documentReference;
	}

	/**
	 * @return the issuerParty
	 */
	public IssuerParty getIssuerParty() {
		return IssuerParty;
	}

	/**
	 * @param issuerParty the issuerParty to set
	 */
	public void setIssuerParty(IssuerParty issuerParty) {
		IssuerParty = issuerParty;
	}

}
