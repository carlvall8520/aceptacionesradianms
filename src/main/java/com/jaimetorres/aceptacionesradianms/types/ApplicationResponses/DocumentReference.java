package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class DocumentReference {

	public String ID;
	public UUID UUID;
	public int DocumentTypeCode;

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the uUID
	 */
	public UUID getUUID() {
		return UUID;
	}

	/**
	 * @param uUID the uUID to set
	 */
	public void setUUID(UUID uUID) {
		UUID = uUID;
	}

	/**
	 * @return the documentTypeCode
	 */
	public int getDocumentTypeCode() {
		return DocumentTypeCode;
	}

	/**
	 * @param documentTypeCode the documentTypeCode to set
	 */
	public void setDocumentTypeCode(int documentTypeCode) {
		DocumentTypeCode = documentTypeCode;
	}

}
