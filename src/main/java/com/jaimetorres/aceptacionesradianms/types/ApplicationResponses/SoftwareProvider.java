package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class SoftwareProvider {
	public ProviderID ProviderID;
	public SoftwareID SoftwareID;

	/**
	 * @return the providerID
	 */
	public ProviderID getProviderID() {
		return ProviderID;
	}

	/**
	 * @param providerID the providerID to set
	 */
	public void setProviderID(ProviderID providerID) {
		ProviderID = providerID;
	}

	/**
	 * @return the softwareID
	 */
	public SoftwareID getSoftwareID() {
		return SoftwareID;
	}

	/**
	 * @param softwareID the softwareID to set
	 */
	public void setSoftwareID(SoftwareID softwareID) {
		SoftwareID = softwareID;
	}

	@Override
	public String toString() {
		return "SoftwareProvider [ProviderID=" + ProviderID + ", SoftwareID=" + SoftwareID + "]";
	}

}
