package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class IdentificationCode {

	public int listAgencyID;
	public String listAgencyName;
	public String listSchemeURI;
	public String text;

	/**
	 * @return the listAgencyID
	 */
	public int getListAgencyID() {
		return listAgencyID;
	}

	/**
	 * @param listAgencyID the listAgencyID to set
	 */
	public void setListAgencyID(int listAgencyID) {
		this.listAgencyID = listAgencyID;
	}

	/**
	 * @return the listAgencyName
	 */
	public String getListAgencyName() {
		return listAgencyName;
	}

	/**
	 * @param listAgencyName the listAgencyName to set
	 */
	public void setListAgencyName(String listAgencyName) {
		this.listAgencyName = listAgencyName;
	}

	/**
	 * @return the listSchemeURI
	 */
	public String getListSchemeURI() {
		return listSchemeURI;
	}

	/**
	 * @param listSchemeURI the listSchemeURI to set
	 */
	public void setListSchemeURI(String listSchemeURI) {
		this.listSchemeURI = listSchemeURI;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "IdentificationCode [listAgencyID=" + listAgencyID + ", listAgencyName=" + listAgencyName
				+ ", listSchemeURI=" + listSchemeURI + ", text=" + text + "]";
	}

}
