package com.jaimetorres.aceptacionesradianms.types.ApplicationResponses;

public class UUID {
	public int schemeID;
	public String schemeName;
	public String text;

	/**
	 * @return the schemeID
	 */
	public int getSchemeID() {
		return schemeID;
	}

	/**
	 * @param schemeID the schemeID to set
	 */
	public void setSchemeID(int schemeID) {
		this.schemeID = schemeID;
	}

	/**
	 * @return the schemeName
	 */
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName the schemeName to set
	 */
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "UUID [schemeID=" + schemeID + ", schemeName=" + schemeName + ", text=" + text + "]";
	}

}
