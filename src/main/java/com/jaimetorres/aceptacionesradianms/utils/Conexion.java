package com.jaimetorres.aceptacionesradianms.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Conexion {

	private static final Logger LOGGER = LoggerFactory.getLogger(Conexion.class);

	static Connection conexiones;

	public static void close(ResultSet resulSet) {
		try {
			resulSet.close();
			LOGGER.info("CERRANDO RESULSET");
		} catch (Exception e) {
			LOGGER.info("EXEPCION EN METODO CERRAR RESULTSET: " + e.toString());
		}
	}

	public static void close(PreparedStatement stmt) {
		try {
			stmt.close();
			LOGGER.info("CERRANDO STATEMENT");
		} catch (Exception e) {
			LOGGER.info("EXEPCION EN METODO CERRAR STATEMENT: " + e.toString());
		}
	}

	public static void close(Connection conex) {
		try {
			conex.close();
			LOGGER.info("CERRANDO CONEXION");
		} catch (Exception e) {
			LOGGER.info("EXEPCION EN METODO CERRAR STATEMENT: " + e.toString());
		}
	}

}
