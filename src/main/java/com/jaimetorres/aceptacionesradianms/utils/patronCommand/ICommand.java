package com.jaimetorres.aceptacionesradianms.utils.patronCommand;

import com.jaimetorres.aceptacionesradianms.utils.exception.BusinessException;

public interface ICommand {
	
	Object execute(IParameter parameter) throws BusinessException;

	void undo();

}
